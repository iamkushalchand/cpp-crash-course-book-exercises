#include <cstdio>

int sum(int num1, int num2)
{
	return num1+num2;
}


int main()
{
	int n1 = 10;
	int n2 = 20;

	printf("The sum of %d, %d is: %d.\n", n1, n2, sum(n1,n2));
	return 0;
}

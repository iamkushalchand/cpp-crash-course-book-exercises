#include <cstdio>

int absolute_value(int num)
{
	if (num >=0)
		return num;
	else
		return -num;
}

int main()
{
	int my_num = -10;
	printf("The absolute value of %d is %d.\n", my_num, absolute_value(my_num));
	return 0;
}
